# Contact Dropbox #

This is a generic service which can be used for any website's contact drop. Name, phone, email and message are supported as shown below.

![Screen Shot 2017-07-06 at 7.35.07 AM.png](https://bitbucket.org/repo/baqjEr4/images/4169104659-Screen%20Shot%202017-07-06%20at%207.35.07%20AM.png)

Same instance of service can be used with multiple websites. There is a field called **tags** is kept to identify them. I used my website's domain name as tag in order to identify.

New features in development. 

1. ** Subscribe email notification ** A tag will be mapped with a list of email ids to notify. A notification will be sent to these ids. So whenever someone sends a message, email will be received. This will save daily lookup of new contact drops explicitly.