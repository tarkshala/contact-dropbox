package com.inbhiwadi.sns;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.inbhiwadi.aws.ContactAWSCredentialsProvider;
import lombok.extern.slf4j.Slf4j;

import java.util.ResourceBundle;

/**
 * Publishes notification to aws SNS
 *
 * Created by yadavkul on 09/07/17.
 */
@Slf4j
public class NotificationPublisher {

    /**
     * Publish a notification.
     *  1. Email
     *
     * @param msg to be published
     */
    public void publish(String msg) {
        final ResourceBundle bundle = ResourceBundle.getBundle("project-config");
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(bundle.getString("accessKey"), bundle.getString("secretKey"));
        AWSCredentialsProvider contactAWSCredentialsProvider = new ContactAWSCredentialsProvider(awsCredentials);
        AmazonSNSClientBuilder snsClientBuilder = AmazonSNSClient.builder();
        snsClientBuilder.setRegion(Regions.AP_SOUTH_1.getName());
        snsClientBuilder.setCredentials(contactAWSCredentialsProvider);
        AmazonSNSClient snsClient = (AmazonSNSClient) snsClientBuilder.build();
        PublishRequest request = new PublishRequest(bundle.getString("topicARN"), msg);
        log.info("Publishing notification message [{}]", msg);
        PublishResult response = snsClient.publish(request);
        log.info("sns response : [{}]", response.toString());
    }
}
