package com.inbhiwadi.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.HashMap;
import java.util.Map;

/**
 * Contact data object.
 *
 * Created by yadavkul on 24/03/17.
 */
@DynamoDBTable(tableName="Contacts")
public class Contact {

    private String name;

    private String phone;

    private String email;

    private String message;

    private Map<String, String> tags;

    public Contact() {
        this.tags = new HashMap<String, String>();
    }

    @DynamoDBAttribute(attributeName = "Name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @DynamoDBHashKey(attributeName="Phone")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @DynamoDBAttribute(attributeName = "Email")
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @DynamoDBAttribute(attributeName = "Message")
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    @DynamoDBAttribute(attributeName = "Tags")
    public Map<String, String> getTags() {
        return tags;
    }

    public void addTag(String tagName, String tagValue) {
        this.tags.put(tagName, tagValue);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name : " + name);
        builder.append("\n");
        builder.append("Phone : " + phone);
        builder.append("\n");
        builder.append("email : " + email);
        builder.append("\n");
        builder.append("message : " + message);
        builder.append("\n");
        return builder.toString();
    }

}
