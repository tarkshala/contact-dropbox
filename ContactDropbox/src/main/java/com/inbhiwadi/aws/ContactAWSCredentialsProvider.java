package com.inbhiwadi.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;

/**
 * Credential provider to access aws account.
 *
 * Created by kuldeep on 25/03/17.
 */
public class ContactAWSCredentialsProvider implements AWSCredentialsProvider {

    private AWSCredentials credentials;

    public ContactAWSCredentialsProvider(AWSCredentials credentials){
        this.credentials = credentials;
    }

    @Override
    public AWSCredentials getCredentials() {
        return credentials;
    }

    @Override
    public void refresh() {
    }
}
