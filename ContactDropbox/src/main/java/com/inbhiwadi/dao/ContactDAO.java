package com.inbhiwadi.dao;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.inbhiwadi.aws.ContactAWSCredentialsProvider;
import com.inbhiwadi.model.Contact;
import lombok.extern.slf4j.Slf4j;

import java.util.ResourceBundle;

/**
 * DAO for {@link Contact}
 *
 *
 * Created by yadavkul on 24/03/17.
 */
@Slf4j
public class ContactDAO {

    /**
     * Persist the contact in database
     * @param contact
     */
    public void save(Contact contact) {
        final ResourceBundle bundle = ResourceBundle.getBundle("project-config");
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(bundle.getString("accessKey"), bundle.getString("secretKey"));
        AWSCredentialsProvider contactAWSCredentialsProvider = new ContactAWSCredentialsProvider(awsCredentials);
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.AP_SOUTH_1);
        builder.setCredentials(contactAWSCredentialsProvider);

        AmazonDynamoDB dynamoDB = builder.build();

        DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(dynamoDB);
        log.info("Saving contact: [{}]", contact.getName());
        dynamoDBMapper.save(contact);
    }
}
