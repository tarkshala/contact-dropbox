package com.inbhiwadi.services;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.inbhiwadi.dao.ContactDAO;
import com.inbhiwadi.model.Contact;
import com.inbhiwadi.sns.NotificationPublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 * Webservice for Contacts
 *
 * Created by yadavkul on 24/03/17.
 */
@Path("/contact")
@Slf4j
@Singleton
public class ContactDropboxService {

    private ContactDAO contactDAO;

    private NotificationPublisher notificationPublisher;

    public ContactDropboxService() {
        ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        this.contactDAO = (ContactDAO) context.getBean("contactDAO");
        this.notificationPublisher = (NotificationPublisher) context.getBean("notificationPublisher");
    }

    @GET
    public String greet() throws InterruptedException {
        log.info("Welcome to InBhiwadi contact services! {}", Thread.currentThread().getId());
        return "Welcome to InBhiwadi contact services";
    }

    @POST
    @Path("/drop")
    public Response create(Contact contact) {

        log.debug("Received contact is : [{}]", contact.toString());
        contactDAO.save(contact);
        notificationPublisher.publish(contact.toString());
        return Response.accepted("Contact dropped in box").build();
    }
}
